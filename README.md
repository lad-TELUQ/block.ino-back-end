# block.ino - Back-End


[block.ino project](http://blockino.ufsc.br/) was orignaly developed at [Remote Experimentation Laboratory (RExLab)](https://rexlab.ufsc.br) in Brazil in 2016.

The front-end application has been upload to a [GitLab repository](https://gitlab.com/lad-TELUQ/block.ino-front-end). This repository describes the back-end app.

### Requirements

* Node.js 0.12 or higher
* "express": "^4.13.3"
* "serialport": "^2.0.6"
* "socket.io": "^1.4.5"
* Arduino IDE ^1.6
* Makefile for Arduino Sketches
* Unix based OS

## Cloning and setting this project

This project can be cloned using CLI with the following command.

```sh
$ git clone https://gitlab.com/lad-TELUQ/block.ino-back-end
```

Node.js depencies must be installed running the following command on the project root folder.

```sh
$ npm install
```

### Makefile for Arduino Sketches

### Requirements

### Arduino IDE

You need to have the Arduino IDE. You can either install it through the
installer or download the distribution zip file and extract it.

### Usage

You can also find more [detailed instructions in this guide](http://hardwarefun.com/tutorials/compiling-arduino-sketches-using-makefile).

On the Mac you might want to set:

```make
    ARDUINO_DIR   = /Applications/Arduino.app/Contents/Resources/Java
    ARDMK_DIR     = /usr/local
    AVR_TOOLS_DIR = /usr
    MONITOR_PORT  = /dev/ttyACM0
    BOARD_TAG     = mega2560
```

On Linux (if you have installed through package), you shouldn't need to set anything other than your board type and port:

```make
    BOARD_TAG     = mega2560
    MONITOR_PORT  = /dev/ttyACM0
```

- `BOARD_TAG` - Type of board, for a list see boards.txt or `make show_boards`
- `MONITOR_PORT` - The port where your Arduino is plugged in, usually `/dev/ttyACM0` or `/dev/ttyUSB0` in Linux or Mac OS X and `com3`, `com4`, etc. in Windows.
- `ARDUINO_DIR` - Path to Arduino installation. In Cygwin in Windows this path must be
  relative, not absolute (e.g. "../../arduino" and not "/c/cygwin/Arduino").
- `ARDMK_DIR`   - Path where the `*.mk` are present. If you installed the package, then it is usually `/usr/share/arduino`
- `AVR_TOOLS_DIR` - Path where the avr tools chain binaries are present. If you are going to use the binaries that came with Arduino installation, then you don't have to set it. Otherwise set it realtive and not absolute.

The list of all variables that can be overridden is available at [arduino-mk-vars.md](arduino-mk-vars.md) file.

### Setting BOARD_TAG

The `BOARD_TAG` must be setted on ```MakeFile``` on [line 554](https://gitlab.com/lad-TELUQ/block.ino-back-end/blob/master/Makefile#L554). To check which value set, it's highly recommended execute the command `make show_boards`.


### Including Libraries

### Runing the project

To execute the application the user can execute using CLI with the following command:

```
node apps.js
```

## Production environment

We strongly recommend users to use [Forever NPM package](https://www.npmjs.com/package/forever) to create an application deamon.

Users must install Forever executing the following command:

```
# npm install -g forever
```

The following script must be created on `/etc/init.d/` direcotory.

#### TODO

* You must edit fields after “### BEGIN INIT INFO”
* Rename your file without any extension, for example `lab`
* Set the file permisions to 755 running the following command
```sh
# chmod 755 scriptname
```
* Installing your script to automaticaly run after boot
```sh
update-rc.d scriptname defaults 
````
* Uninstalling you script
```sh
update-rc.d -f scriptname remove
```


## License

This project was based on [Sudar Arduino-Makefile](https://github.com/sudar/Arduino-Makefile/) and is shared under MIT license.

